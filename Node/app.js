require('dotenv').config();

const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const express = require('express');
const cors = require('cors');
const router = require('./routes/router');

const app = express();
const db = mongoose.connection; 

// DB
mongoose.connect(process.env.DATABASE_URL, { useNewUrlParser: true });
db.on('error', (error) => console.log(error));
db.once('open', () => console.log('Connected to Database'));
// 

const corsOptions ={
    origin:'http://localhost:3000', 
    credentials:true,            
}

app.use(cors(corsOptions));

app.use(express.json());
app.use(express.static("public"));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use('/', router);

app.listen(process.env.PORT, () => console.log("===>>>", process.env.PORT, "<<<==="));