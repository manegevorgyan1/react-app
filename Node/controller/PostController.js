require('dotenv').config();
const jwt = require('jsonwebtoken');
const User = require("../model/user");

class PostController {

  static async register(req, res) {
    // await User.find()
    const user = await new User({
      name: req.body.name,
      surname: req.body.surname,
      email: req.body.email,
      password: req.body.password,
    });

    try {
      const newUser = await user.save()
      res.status(201).json(newUser)
    } catch (err) {
      res.status(400).json({ message: err.message })
    }

    // console.log(user, "jhgh");
    // res.send({ user, status: "User Registered" });
  }

  static async getAllUsers(req, res) {
    const users = await User.find();
    res.send(users);
  }

  static async login(req, res) {

    // const user = await User.findOne({
    //   where: {
    //     email: req.body.email,
    //   }
    // })
    // if(user.email == req.body.email) {
    //   console.log("Right Email");
    // } else {
    //   console.log('Wrong Email')
    // }
    // if(user.password == req.body.password){
    //   console.log('Right Password')
    // } else {
    //   console.log('Wrong Password')
    // }

    // sa der sxal a ashxatum

    const username = req.body.email;
    const user = { name: username };

    const accessToken = generateAccessToken(user);
    const refreshToken = jwt.sign(user, process.env.REFRESH_TOKEN_SECRET)

    res.json({ accessToken, refreshToken })

    function generateAccessToken(user) {
      return jwt.sign(user, process.env.ACCESS_TOKEN_SECRET, { expiresIn: '15s' })
    }
    // https://github.com/WebDevSimplified/JWT-Authentication/blob/master/authServer.js
  }


}

module.exports = PostController;
