const express = require("express");
const PostController = require("../controller/PostController");
const router = express.Router();

router.use(express.json());

//
// router.post("/login", authenticateToken, (req, res) => {
//   // Authenticate User

//   const username = req.body.username;
//   const user = { name: username };

//   const accessToken = jwt.sign(user, process.env.ACCESS_TOKEN_SECRET);
//   res.json({ accessToken });
// });
//




router.get('/getAllUsers', PostController.getAllUsers);

router.post("/register", PostController.register);
router.post('/login', PostController.login);




module.exports = router;