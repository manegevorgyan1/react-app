import { Link } from "react-router-dom";
import { Menu } from "../../layouts/header/Menu";
import "./home.css";
import "./@media.css";

export const Home = () => {
  const categories = [
    {
      image: require("../../assets/images/beverage 1.png"),
      title: "Beverages",
      path: "#",
    },
    {
      image: require("../../assets/images/milk 1.png"),
      title: "Dairy",
      path: "#",
    },
    {
      image: require("../../assets/images/meat 1.png"),
      title: "Meat",
      path: "#",
    },
    {
      image: require("../../assets/images/seasoning 1.png"),
      title: "Spices & Herbs",
      path: "#",
    },
    {
      image: require("../../assets/images/stationery 1.png"),
      title: "Stationery",
      path: "#",
    },
    {
      image: require("../../assets/images/cosmetics 1.png"),
      title: "Cosmetics",
      path: "#",
    },
  ];

  const offers = [
    {
      image: require("../../assets/images/Rectangle 29.png"),
      title: "Cottage Cheese",
      path: "#",
      capacity: "3 Kg",
      currentPrice: "$100",
      previousPrice: "$150",
    },
    {
      image: require("../../assets/images/kaxamb.png"),
      title: "Cabbage",
      path: "#",
      capacity: "1 Kg",
      currentPrice: "$20",
      previousPrice: "$40",
    },
    {
      image: require("../../assets/images/the-creative-exchange-juBur46D3VI-unsplash-removebg-preview 1.png"),
      title: "Farmer’s Milk",
      path: "#",
      capacity: "1 Litre",
      currentPrice: "$20",
      previousPrice: "$40",
    },
    {
      image: require("../../assets/images/pexels-mart-production-8217467-removebg-preview 1.png"),
      title: "Eco-Green Shampoo",
      path: "#",
      capacity: "1 Litre",
      currentPrice: "$40",
      previousPrice: "$60",
    },
  ];

  const bestsellers = [
    {
      image: require("../../assets/images/Rectangle 36.png"),
      title: "Ice Cream Sundae",
      price: "$20",
    },
    {
      image: require("../../assets/images/pexels-karolina-grabowska-4735904-removebg-preview 1.png"),
      title: "Eco- Face cream",
      price: "$120",
    },
    {
      image: require("../../assets/images/sincerely-media-55u9sVVJ4p8-unsplash-removebg-preview 1.png"),
      title: "Hand Sanitizer",
      price: "$20",
    },
    {
      image: require("../../assets/images/IMG-0258-removebg-preview 1.png"),
      title: "Beef Steak",
      price: "$20",
    },
  ];

  const popular = [
    {
      image: require("../../assets/images/pexels-mart-production-8217467-removebg-preview 1.png"),
      title: "Eco-Shampoo",
      path: "#",
      price: "$50",
    },
    {
      image: require("../../assets/images/kaxamb.png"),
      title: "Cabbage",
      path: "#",
      price: "$20",
    },
    {
      image: require("../../assets/images/sincerely-media-55u9sVVJ4p8-unsplash-removebg-preview 1.png"),
      title: "Hand Sanitizer",
      path: "#",
      price: "$20",
    },
    {
      image: require("../../assets/images/IMG-0258-removebg-preview 1.png"),
      title: "Beef Steak",
      path: "#",
      price: "$20",
    },
  ];

  return (
    <>
      <Menu />

      {/* HEADER */}
      <header>
        <img src={require("../../assets/images/round.png")} id="round" />
        <img
          src={require("../../assets/images/vegetables.png")}
          id="vegetables"
        />
        <img src={require("../../assets/images/basket.png")} id="basket" />

        <div>
          <span id="lorem">Lorem Ipsum Dolor Sit</span>
          <h1 id="ipsum">
            Lorem Ipsum Dolor Sit Amet, Consectetur Adipisicing Elit.
          </h1>
          <span className="signupBtn">
            <Link to="/login" className="insideBtn" id="headerSignupBtn">
              Sign Up
            </Link>
          </span>
        </div>
      </header>

      <div className="container">
        {/* SECTION 1 */}
        <section className="section" id="sec1">
          <h1 id="categoriesTitle" className="title">
            Categories
          </h1>
          <div className="categoryContainer">
            {categories.map((e, i) => (
              <div key={i} className="categoryItems">
                <img src={e.image} alt={e.title} />
                <h1>
                  <Link to={e.path}>{e.title}</Link>
                </h1>
              </div>
            ))}
          </div>

          <div className="arrow-title">
            <h1 id="offersTitle" className="title">
              Offers
            </h1>
            <div className="arrows">
              <span>
                <Link to="#">
                  <img
                    src={require("../../assets/images/right-arrow 2.png")}
                    alt="Left Arrow"
                    id="Larrow"
                  />
                </Link>
              </span>

              <span>
                <Link to="#">
                  <img
                    src={require("../../assets/images/right-arrow 2.png")}
                    alt="Left Arrow"
                    id="Rarrow"
                  />
                </Link>
              </span>
            </div>
          </div>

          <div className="container">
            {offers.map((e, i) => (
              <div key={i} className="containerItems">
                <img src={e.image} alt={e.title} />

                <div className="details">
                  <h1>
                    <Link to={e.path}>{e.title}</Link>
                  </h1>
                  <span className="capacity">{e.capacity}</span>
                  <span className="currPrice">{e.currentPrice}</span>
                  <span className="prevPrice">{e.previousPrice}</span>
                </div>
              </div>
            ))}
          </div>
        </section>

        {/* SECTION 2 */}
        <section className="section">
          <div className="arrow-title">
            <h1 id="bestsellerTitle" className="title">
              Best Selling Products
            </h1>
            <div className="arrows">
              <span>
                <Link to="#">
                  <img
                    src={require("../../assets/images/right-arrow 2.png")}
                    alt="Left Arrow"
                    id="Larrow"
                  />
                </Link>
              </span>

              <span>
                <Link to="#">
                  <img
                    src={require("../../assets/images/right-arrow 2.png")}
                    alt="Left Arrow"
                    id="Rarrow"
                  />
                </Link>
              </span>
            </div>
          </div>

          <div className="container">
            {bestsellers.map((e, i) => (
              <div key={i} className="containerItems">
                <img src={e.image} alt={e.title} />

                <div className="details">
                  <h1>{e.title}</h1>
                  <span className="currPrice">{e.price}</span>

                  <div className="add">
                    <input type="number" placeholder="1" className="inp" />
                    <span>
                      <Link to="#">
                        <img
                          src={require("../../assets/images/add-to-basket 1.png")}
                          className="addToCart"
                        />
                      </Link>
                    </span>
                  </div>
                </div>
              </div>
            ))}
          </div>
        </section>

        {/* SECTION 3 */}
        <section className="section">
          <div className="arrow-title">
            <h1 className="title">Popular Products</h1>
            <div className="arrows">
              <span>
                <Link to="#">
                  <img
                    src={require("../../assets/images/right-arrow 2.png")}
                    alt="Left Arrow"
                    id="Larrow"
                  />
                </Link>
              </span>

              <span>
                <Link to="#">
                  <img
                    src={require("../../assets/images/right-arrow 2.png")}
                    alt="Left Arrow"
                    id="Rarrow"
                  />
                </Link>
              </span>
            </div>
          </div>
          <div className="container">
            {bestsellers.map((e, i) => (
              <div key={i} className="containerItems">
                <img src={e.image} alt={e.title} />

                <div className="details">
                  <h1>{e.title}</h1>
                  <span className="currPrice">{e.price}</span>

                  <div className="add">
                    <input type="number" placeholder="1" className="inp" />
                    <span>
                      <Link to="#">
                        <img
                          src={require("../../assets/images/add-to-basket 1.png")}
                          className="addToCart"
                        />
                      </Link>
                    </span>
                  </div>
                </div>
              </div>
            ))}
          </div>
        </section>

        {/* SECTION 4 */}
        <section className="section4">
          <div>
            <img
              src={require("../../assets/images/vegetables-fruits-arrangement-removebg-preview 1.png")}
            />

            <div className="innerFlex">
              <h1>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h1>
              <span className="sub">
                Subscribe to get 10% Discount and promotion
              </span>
              <input type="email" placeholder="Email address" />

              <span className="signupBtn">
                <Link to="/login" className="insideBtn" id="section4SignupBtn">
                  Sign Up
                </Link>
              </span>
            </div>
          </div>
        </section>
      </div>

      {/* FOOTER */}
      <footer className="footer">
        <div className="l-footer">
          <ul>
            <li>
              <h2>Services</h2>
            </li>
            <li>
              <Link to="#">Email Marketing</Link>
            </li>
            <li>
              <Link to="#">Campaigns</Link>
            </li>
            <li>
              <Link to="#">Branding</Link>
            </li>
            <li>
              <Link to="#">Offline</Link>
            </li>
          </ul>

          <ul>
            <li>
              <h2>About</h2>
            </li>
            <li>
              <Link to="#">Our Story</Link>
            </li>
            <li>
              <Link to="#">Benefits</Link>
            </li>
            <li>
              <Link to="#">Team</Link>
            </li>
            <li>
              <Link to="#">Careers</Link>
            </li>
          </ul>

          <ul>
            <li>
              <h2>Help</h2>
            </li>
            <li>
              <Link to="#">FAQs</Link>
            </li>
            <li>
              <Link to="#">Contact Us</Link>
            </li>
          </ul>

          <img src={require("../../assets/images/logo.png")} alt="logo" />
        </div>

        <div className="r-footer">
          <div className="terms">
            <span>
              <Link to="#">Terms & Conditions</Link>
            </span>
            <span>
              <Link to="#">Privacy Policy</Link>
            </span>
          </div>

          <div className="icons">
            <Link to="#">
              <img
                src={require("../../assets/images/001-facebook.png")}
                alt="fb"
              />
            </Link>
            <Link to="#">
              <img
                src={require("../../assets/images/003-twitter.png")}
                alt="tw"
              />
            </Link>
            <Link to="#">
              <img
                src={require("../../assets/images/004-instagram.png")}
                alt="ig"
              />
            </Link>
          </div>
        </div>
      </footer>
    </>
  );
};

// titlenery arandzin en, petqa xmbavorel