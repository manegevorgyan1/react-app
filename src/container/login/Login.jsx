import { Link } from "react-router-dom";
import { Menu } from "../../layouts/header/Menu";

export const Login = () => {
  return (
    <>
    <Menu/>
    <div className="loginBody">

      <div className="leftPart">

        <h1 className="signin">Sign in</h1>
        <p className="secondary" id="newUser">New User? <span><Link to='#'>Create account</Link></span></p>

        <label className="secondary" id="phone">Your phone number*</label>
        <input className="input" id="phoneInput" type="number" required placeholder="+254" />

        <label className="secondary" id="password">Enter password*</label>
        <input className="input" id="passwordInput" type="password" placeholder="Enter password" />

        <label className="secondary" id="forgotPswd"><a href="#">Forgot password?</a></label>

        <div id="agreeCheckbox" className="secondary"><input type="checkbox" defaultChecked /> I agree to terms & conditions </div> 

        <button className="greenBtn">Sign in</button>
        
      </div>

      <div className="rightPart">
        <h1>Create Account</h1>
        <p className="secondary">Create account to manage orders</p>
        <button className="greenBtn">Create Account</button>
      </div>

      <span className="secondary" id="policy"><Link to='#'>Term of Use. Privacy Policy</Link></span>

    </div>
    </>
  );
};
