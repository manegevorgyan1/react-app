import { Link } from 'react-router-dom';

export const Menu = () => {

    const navItems = [
      {
        menuItem: "Shop",
        path: "#",
        className: "",
        id: ""
      },
      {
        menuItem: "Orders",
        path: "#",
        className: "",
        id: ""
      },
      {
        menuItem: "Cart",
        path: "#",
        className: "",
        id: ""
      },
      {
        menuItem: "Contact",
        path: "#",
        className: "",
        id: ""
      },
    ]

    const navButtons = [
      {
        menuItem: "Sign up",
        path: "#",
        className: "navBtns",
        id: "signup"
      },
      {
        menuItem: "Login",
        path: "/login",
        className: "navBtns",
        id: "login"
      },
    ]

  return (
    <>

      <nav className='nav'>
        <Link to={'/'}><img className='logo' src={require('../../assets/images/logo.png')} alt="logo" /></Link>

        {/* PC */}
        <ul>
        {
          navItems.map((e, i) => (
            <li key={i} className={e.className} id={e.id}><Link to={e.path}>{e.menuItem}</Link></li>
          ))
        }
        {
          navButtons.map((e, i) => (
            <li key={i}><div className={e.className} id={e.id}><Link to={e.path}>{e.menuItem}</Link></div></li>
          ))
        }
        </ul>
      </nav>

      {/* Phone */}

      <nav className="navbar navbar-expand-sm bg-muted navbar-dark">
        <div className="container-fluid">
          <Link to={'/'}><img className='navbar-brand' src={require('../../assets/images/logo.png')} alt="menu" /></Link>
          <button className="navbar-toggler bg-success" type="button" data-bs-toggle="collapse" data-bs-target="#collapsibleNavbar">
            <span className="navbar-toggler-icon" ></span>
          </button>
          <div className="collapse navbar-collapse bg-success" id="collapsibleNavbar">
            <ul className="navbar-nav">
              {
                navItems.map((e, i) => (
                  <li key={i} className='nav-item' id={e.id}><Link to={e.path} className='nav-link'>{e.menuItem}</Link></li>
                ))
              }
              {
                navButtons.map((e, i) => (
                  <li key={i} className='nav-item' id={e.id}><Link to={e.path} className='nav-link'>{e.menuItem}</Link></li>
                ))
              }
            </ul>
          </div>
        </div>
      </nav>
    </>
  );
};
