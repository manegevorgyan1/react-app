import { BrowserRouter, Route, Routes } from "react-router-dom";
import { Home } from "../container/homePage/Home.jsx";
import { Login } from "../container/login/Login.jsx";

export const MyRouter=()=>{
    return (<div>
        <BrowserRouter>
        <Routes>
            <Route path = "/" element={<Home/>} />
            <Route path = "/login" element={<Login/>} />
        </Routes>
        </BrowserRouter>
    </div>)
}
